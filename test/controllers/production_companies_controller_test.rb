require 'test_helper'

class ProductionCompaniesControllerTest < ActionController::TestCase
  setup do
    @production_company = production_companies(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:production_companies)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create production_company" do
    assert_difference('ProductionCompany.count') do
      post :create, production_company: { name: @production_company.name }
    end

    assert_redirected_to production_company_path(assigns(:production_company))
  end

  test "should show production_company" do
    get :show, id: @production_company
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @production_company
    assert_response :success
  end

  test "should update production_company" do
    patch :update, id: @production_company, production_company: { name: @production_company.name }
    assert_redirected_to production_company_path(assigns(:production_company))
  end

  test "should destroy production_company" do
    assert_difference('ProductionCompany.count', -1) do
      delete :destroy, id: @production_company
    end

    assert_redirected_to production_companies_path
  end
end
