require 'test_helper'

class MovieActorsControllerTest < ActionController::TestCase
  setup do
    @movie_actor = movie_actors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:movie_actors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create movie_actor" do
    assert_difference('MovieActor.count') do
      post :create, movie_actor: { actor_id: @movie_actor.actor_id, movie_id: @movie_actor.movie_id }
    end

    assert_redirected_to movie_actor_path(assigns(:movie_actor))
  end

  test "should show movie_actor" do
    get :show, id: @movie_actor
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @movie_actor
    assert_response :success
  end

  test "should update movie_actor" do
    patch :update, id: @movie_actor, movie_actor: { actor_id: @movie_actor.actor_id, movie_id: @movie_actor.movie_id }
    assert_redirected_to movie_actor_path(assigns(:movie_actor))
  end

  test "should destroy movie_actor" do
    assert_difference('MovieActor.count', -1) do
      delete :destroy, id: @movie_actor
    end

    assert_redirected_to movie_actors_path
  end
end
