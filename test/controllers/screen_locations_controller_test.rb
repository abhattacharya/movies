require 'test_helper'

class ScreenLocationsControllerTest < ActionController::TestCase
  setup do
    @screen_location = screen_locations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:screen_locations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create screen_location" do
    assert_difference('ScreenLocation.count') do
      post :create, screen_location: { address: @screen_location.address, city_id: @screen_location.city_id, latitude: @screen_location.latitude, longitude: @screen_location.longitude, name: @screen_location.name, neighbourhood: @screen_location.neighbourhood }
    end

    assert_redirected_to screen_location_path(assigns(:screen_location))
  end

  test "should show screen_location" do
    get :show, id: @screen_location
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @screen_location
    assert_response :success
  end

  test "should update screen_location" do
    patch :update, id: @screen_location, screen_location: { address: @screen_location.address, city_id: @screen_location.city_id, latitude: @screen_location.latitude, longitude: @screen_location.longitude, name: @screen_location.name, neighbourhood: @screen_location.neighbourhood }
    assert_redirected_to screen_location_path(assigns(:screen_location))
  end

  test "should destroy screen_location" do
    assert_difference('ScreenLocation.count', -1) do
      delete :destroy, id: @screen_location
    end

    assert_redirected_to screen_locations_path
  end
end
