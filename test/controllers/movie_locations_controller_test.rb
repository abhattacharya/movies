require 'test_helper'

class MovieLocationsControllerTest < ActionController::TestCase
  setup do
    @movie_location = movie_locations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:movie_locations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create movie_location" do
    assert_difference('MovieLocation.count') do
      post :create, movie_location: { location_id: @movie_location.location_id, movie_id: @movie_location.movie_id }
    end

    assert_redirected_to movie_location_path(assigns(:movie_location))
  end

  test "should show movie_location" do
    get :show, id: @movie_location
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @movie_location
    assert_response :success
  end

  test "should update movie_location" do
    patch :update, id: @movie_location, movie_location: { location_id: @movie_location.location_id, movie_id: @movie_location.movie_id }
    assert_redirected_to movie_location_path(assigns(:movie_location))
  end

  test "should destroy movie_location" do
    assert_difference('MovieLocation.count', -1) do
      delete :destroy, id: @movie_location
    end

    assert_redirected_to movie_locations_path
  end
end
