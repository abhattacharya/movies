class CreateNeighbourhoods < ActiveRecord::Migration
  def change
    create_table :neighbourhoods do |t|
      t.string :title
      t.belongs_to :city
      t.timestamps null: false
    end
  end
end
