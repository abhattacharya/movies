class CreateMovieLocations < ActiveRecord::Migration
  def change
    create_table :movie_locations do |t|
      t.belongs_to :movie, index: true
      t.belongs_to :screen_location, index: true

      t.timestamps null: false
    end
  end
end
