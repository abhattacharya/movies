class CreateScreenLocations < ActiveRecord::Migration
  def change
    create_table :screen_locations do |t|
      t.string :name
      t.string :address
      t.float :latitude
      t.float :longitude
      t.belongs_to :neighbourhood

      t.timestamps null: false
    end
  end
end
