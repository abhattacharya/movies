class CreateMovieActors < ActiveRecord::Migration
  def change
    create_table :movie_actors do |t|
      t.belongs_to :movie, index: true
      t.belongs_to :actor, index: true

      t.timestamps null: false
    end
  end
end
