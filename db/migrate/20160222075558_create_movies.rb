class CreateMovies < ActiveRecord::Migration
  def change
    create_table :movies do |t|
      t.string :title
      t.string :release_year
      t.string :writers
      t.integer :production_company_id
      t.string :director
      t.timestamps null: false
    end
  end
end
