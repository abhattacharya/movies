# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160223150406) do

  create_table "actors", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "cities", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "locs", id: false, force: :cascade do |t|
    t.integer "id",       limit: 4
    t.string  "location", limit: 255
    t.string  "lat",      limit: 255
    t.string  "long",     limit: 255
  end

  create_table "locs1", id: false, force: :cascade do |t|
    t.integer "id",        limit: 4
    t.string  "title",     limit: 255
    t.string  "actor_1",   limit: 255
    t.string  "locations", limit: 255
  end

  create_table "movie_actors", force: :cascade do |t|
    t.integer  "movie_id",   limit: 4
    t.integer  "actor_id",   limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "movie_actors", ["actor_id"], name: "index_movie_actors_on_actor_id", using: :btree
  add_index "movie_actors", ["movie_id"], name: "index_movie_actors_on_movie_id", using: :btree

  create_table "movie_locations", force: :cascade do |t|
    t.integer  "movie_id",           limit: 4
    t.integer  "screen_location_id", limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "movie_locations", ["movie_id"], name: "index_movie_locations_on_movie_id", using: :btree
  add_index "movie_locations", ["screen_location_id"], name: "index_movie_locations_on_screen_location_id", using: :btree

  create_table "movies", force: :cascade do |t|
    t.string   "title",                 limit: 255
    t.string   "release_year",          limit: 255
    t.string   "writers",               limit: 255
    t.integer  "production_company_id", limit: 4
    t.string   "director",              limit: 255
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "production_company",    limit: 255
  end

  create_table "neighbourhoods", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.integer  "city_id",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "production_companies", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "screen_locations", force: :cascade do |t|
    t.string   "name",             limit: 255
    t.string   "address",          limit: 255
    t.float    "latitude",         limit: 24
    t.float    "longitude",        limit: 24
    t.integer  "neighbourhood_id", limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "screen_locations", ["name"], name: "unique_location", unique: true, using: :btree

end
