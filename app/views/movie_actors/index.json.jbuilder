json.array!(@movie_actors) do |movie_actor|
  json.extract! movie_actor, :id, :movie_id, :actor_id
  json.url movie_actor_url(movie_actor, format: :json)
end
