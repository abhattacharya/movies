json.array!(@production_companies) do |production_company|
  json.extract! production_company, :id, :name
  json.url production_company_url(production_company, format: :json)
end
