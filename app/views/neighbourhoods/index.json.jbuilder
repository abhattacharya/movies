json.array!(@neighbourhoods) do |neighbourhood|
  json.extract! neighbourhood, :id, :title
  json.url neighbourhood_url(neighbourhood, format: :json)
end
