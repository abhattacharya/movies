json.array!(@screen_locations) do |screen_location|
  json.extract! screen_location, :id, :name, :address, :latitude, :longitude, :city_id, :neighbourhood
  json.url screen_location_url(screen_location, format: :json)
end
