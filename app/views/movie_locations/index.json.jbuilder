json.array!(@movie_locations) do |movie_location|
  json.extract! movie_location, :id, :movie_id, :location_id
  json.url movie_location_url(movie_location, format: :json)
end
