class Movie < ActiveRecord::Base
  #attr_accessible :title
  has_many :movie_actors
  has_many :actors, through: :movie_actors
  
  has_many :movie_locations
  has_many :screen_locations, through: :movie_locations
  
  
  
end
