class Neighbourhood < ActiveRecord::Base
  belongs_to :city
  has_many :screen_locations
end
