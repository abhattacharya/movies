class MovieLocation < ActiveRecord::Base
  belongs_to :movie
  belongs_to :screen_location
end
