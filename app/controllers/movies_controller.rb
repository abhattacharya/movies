require 'open-uri'
class MoviesController < ApplicationController
  before_action :set_movie, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token

  # GET /movies
  # GET /movies.json
  

  # GET /movies/1
  # GET /movies/1.json
  def show
    @zoom_level = 14
    results = Hash.new
    ctr =0
    all_locations = ScreenLocation.includes(:movies).where(:movies => {:id => @movie.id})
    all_locations.each do |location|
    
      if(!location.movies.blank? & !location.latitude.nil? & !location.longitude.nil?)
        new_location = Hash.new
        new_location['name'] = location.name
        new_location['movies'] = Array.new
        location.movies.each do |m|
          text = m.title+","+m.release_year
          new_location['movies'].push(text)
        end
        
        new_location['lat'] = location.latitude
        new_location['long'] = location.longitude
        new_location['id'] = location.id
        results[ctr] = new_location
        ctr = ctr+1
      end
    end
    
    @result_locations = results
  end
  
  def index
    @zoom_level = 14
    results = Hash.new
    ctr = 0
    @filter_ids = Array.new
    params.permit!
    actors = params['actors']
    
    @filter_text = "Showing results for - ";
    if !params['release_year'].blank?

      @filter_ids = Movie.where(:release_year => params['release_year'].split(',')).pluck(:id)
      @filter_text += "Release Year - "+params['release_year']+", "
      p @filter_ids
    end
    if !params['title'].blank?
      @filter_ids = @filter_ids | Movie.where(:title => params['title'].split(',')).pluck(:id)
      @filter_text += "Title - "+params['title']+", "
      p @filter_ids
    end
    if !params['director'].blank?
      @filter_ids = @filter_ids | Movie.where(:director => params['director'].split(',')).pluck(:id)
      @filter_text += "Director - "+params['director']+", "
      p @filter_ids
    end
    if !params['actors'].blank?
      @filter_ids = @filter_ids |  Movie.includes(:actors).where(:actors => {:name => params['actors'].split(',')}).pluck(:id)
      @filter_text += "Actor - "+params['actors']+" "
    end

    p @filter_ids
    if !@filter_ids.blank?
      
      all_locations = ScreenLocation.includes(:movies).where(:movies => {:id => @filter_ids})
    else
      @filter_text += "All Movies"
      all_locations = ScreenLocation.includes(:movies)
    end
    all_locations.each do |location|
    
      if(!location.movies.blank? & !location.latitude.nil? & !location.longitude.nil?)
        new_location = Hash.new
        new_location['name'] = location.name
        new_location['movies'] = Array.new
        location.movies.each do |m|
          text = m.title+","+m.release_year
          new_location['movies'].push(text)
        end
        
        new_location['lat'] = location.latitude
        new_location['long'] = location.longitude
        new_location['id'] = location.id
        results[ctr] = new_location
        ctr = ctr+1
      end
    end
    
    @result_locations = results
        
      
  end

  # GET /movies/new
  def new
    @movie = Movie.new
  end

  # GET /movies/1/edit
  def edit
  end
  
  

  # POST /movies
  # POST /movies.json
  def create
    @movie = Movie.new(movie_params)

    respond_to do |format|
      if @movie.save
        format.html { redirect_to @movie, notice: 'Movie was successfully created.' }
        format.json { render :show, status: :created, location: @movie }
      else
        format.html { render :new }
        format.json { render json: @movie.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /movies/1
  # PATCH/PUT /movies/1.json
  def update
    respond_to do |format|
      if @movie.update(movie_params)
        format.html { redirect_to @movie, notice: 'Movie was successfully updated.' }
        format.json { render :show, status: :ok, location: @movie }
      else
        format.html { render :edit }
        format.json { render json: @movie.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /movies/1
  # DELETE /movies/1.json
  def destroy
    @movie.destroy
    respond_to do |format|
      format.html { redirect_to movies_url, notice: 'Movie was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def search_movie
      @result = Movie.where('title LIKE ?','%'+params[:q]+'%').pluck(:title,:id)
      @results = Array.new
      @result.each do |r|
        
        @results.push({"id" => r[0],"name" => r[0]})
      end
        respond_to do |format|
          format.json { render json: @results }
        end
  end
  
  def search_actor
      @result = Actor.where('name LIKE ?','%'+params[:q]+'%').pluck(:name,:id)
      @results = Array.new
      @result.each do |r|
        @results.push({"id" => r[0],"name" => r[0]})
      end
        respond_to do |format|
          format.json { render json: @results }
        end
  end
  
  def search_director
      @result = Movie.where('director LIKE ?','%'+params[:q]+'%').pluck(:director).uniq
      @results = Array.new
      @result.each do |r|
        @results.push({"id" => r,"name" => r})
      end
        respond_to do |format|
          format.json { render json: @results }
        end
  end

  def search_release_year
      @result = Movie.where('release_year LIKE ?',params[:q]+'%').pluck(:release_year).uniq
      @results = Array.new
      @result.each do |r|
        @results.push({"id" => r,"name" => r})
      end
        respond_to do |format|
          format.json { render json: @results }
        end
  end

  def trial
    search_term = params[:search]
    @result_message = "";
    url = "http://176.58.121.28:9999/solr/movies/select?q="+search_term.sub(' ','+')+"&wt=json&indent=true"
    file = open(url)
    data = JSON.parse file.read
    count = data['response']['numFound'].to_i
    if count > 0
      @result_message =  count.to_s+" results found"
      @movies = Array.new
      @docs = data['response']['docs']
      @docs.each do |d|
        title = d['title']
        id = d['movie_id']
        @movies.push(["id" => id,"title" => title])
      end
    else
      @result_message = "No results found. Try again"
      @movies = nil
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_movie
      @movie = Movie.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def movie_params
      params.require(:movie).permit(:title, :release_year, :writers, :production_company, :director)
    end
end
