class MovieLocationsController < ApplicationController
  before_action :set_movie_location, only: [:show, :edit, :update, :destroy]

  # GET /movie_locations
  # GET /movie_locations.json
  def index
    @movie_locations = MovieLocation.all
  end

  # GET /movie_locations/1
  # GET /movie_locations/1.json
  def show
  end

  # GET /movie_locations/new
  def new
    @movie_location = MovieLocation.new
  end

  # GET /movie_locations/1/edit
  def edit
  end

  # POST /movie_locations
  # POST /movie_locations.json
  def create
    @movie_location = MovieLocation.new(movie_location_params)

    respond_to do |format|
      if @movie_location.save
        format.html { redirect_to @movie_location, notice: 'Movie location was successfully created.' }
        format.json { render :show, status: :created, location: @movie_location }
      else
        format.html { render :new }
        format.json { render json: @movie_location.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /movie_locations/1
  # PATCH/PUT /movie_locations/1.json
  def update
    respond_to do |format|
      if @movie_location.update(movie_location_params)
        format.html { redirect_to @movie_location, notice: 'Movie location was successfully updated.' }
        format.json { render :show, status: :ok, location: @movie_location }
      else
        format.html { render :edit }
        format.json { render json: @movie_location.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /movie_locations/1
  # DELETE /movie_locations/1.json
  def destroy
    @movie_location.destroy
    respond_to do |format|
      format.html { redirect_to movie_locations_url, notice: 'Movie location was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_movie_location
      @movie_location = MovieLocation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def movie_location_params
      params.require(:movie_location).permit(:movie_id, :location_id)
    end
end
