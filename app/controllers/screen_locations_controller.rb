class ScreenLocationsController < ApplicationController
  before_action :set_screen_location, only: [:show, :edit, :update, :destroy]

  # GET /screen_locations
  # GET /screen_locations.json
  def index
    @screen_locations = ScreenLocation.all
  end

  # GET /screen_locations/1
  # GET /screen_locations/1.json
  def show
  end

  # GET /screen_locations/new
  def new
    @screen_location = ScreenLocation.new
  end

  # GET /screen_locations/1/edit
  def edit
  end

  # POST /screen_locations
  # POST /screen_locations.json
  def create
    @screen_location = ScreenLocation.new(screen_location_params)

    respond_to do |format|
      if @screen_location.save
        format.html { redirect_to @screen_location, notice: 'Screen location was successfully created.' }
        format.json { render :show, status: :created, location: @screen_location }
      else
        format.html { render :new }
        format.json { render json: @screen_location.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /screen_locations/1
  # PATCH/PUT /screen_locations/1.json
  def update
    respond_to do |format|
      if @screen_location.update(screen_location_params)
        format.html { redirect_to @screen_location, notice: 'Screen location was successfully updated.' }
        format.json { render :show, status: :ok, location: @screen_location }
      else
        format.html { render :edit }
        format.json { render json: @screen_location.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /screen_locations/1
  # DELETE /screen_locations/1.json
  def destroy
    @screen_location.destroy
    respond_to do |format|
      format.html { redirect_to screen_locations_url, notice: 'Screen location was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_screen_location
      @screen_location = ScreenLocation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def screen_location_params
      params.require(:screen_location).permit(:name, :address, :latitude, :longitude, :city_id, :neighbourhood)
    end
end
