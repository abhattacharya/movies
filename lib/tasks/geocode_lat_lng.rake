require 'open-uri'
task :geocode_lat_lng  => :environment do
  api_key = 'AIzaSyDc0uZR_jQlQUTsJew-uzOz7VEjHjf-TaQ'
  
  
  sl = ScreenLocation.where("latitude is null")
  sl.each do |l|
    url = 'https://maps.googleapis.com/maps/api/geocode/json?address='
    name = l.name
    name = name.partition(' (').first
    
    
    url += name + '&key=' + api_key
    if url.ascii_only?
    file = open(url)
    data = JSON.parse file.read
    if !data['results'].blank?
      lat = data['results'][0]['geometry']['location']['lat']
      lng = data['results'][0]['geometry']['location']['lng']
      l.latitude = lat
      l.longitude = lng
      l.save()
    end
    end
  end

  
end
