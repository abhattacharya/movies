require 'open-uri'
task :fetch_movies  => :environment do
  file = open('https://data.sfgov.org/resource/yitu-d5am.json')
  data = JSON.parse file.read
  data.each do |movie|
    data_param = Hash.new
    data_param['title'] = movie['title']
    data_param['writers'] = movie['writer']
    data_param['director'] = movie['director']
    data_param['release_year'] = movie['release_year']
    data_param['production_company'] = movie['production_company']
    actor_1 = movie['actor_1']
    actor_2 = movie['actor_2']
    actor_3 = movie['actor_3']
    locations = movie['locations']
    if(!locations.nil?)
      locations = locations.sub('&','and')
      locations = locations.sub(' - ',', ' )
    end
       
    movie_exists = Movie.exists?((["title = :u", { u: movie['title'] }]))
    if(!movie_exists)
      m = Movie.new(data_param)
      m.save()
      movie_temp = Movie.where(["title = :u", { u: movie['title'] }]).first
    else
      movie_temp = Movie.where(["title = :u", { u: movie['title'] }]).first
    end
    if(!locations.nil?)
        movie_temp = Movie.where(["title = :u", { u: movie['title'] }]).first
        sl = ScreenLocation.where(["name = :u", { u: locations }]).first
        movie_temp.movie_locations.create(screen_location: sl)
    end
    if(!actor_1.nil?)
      actor_exists = Actor.exists?((["name = :u", { u: actor_1 }]))
      if(actor_exists)
        a = Actor.where(["name = :u", { u: actor_1 }]).first
        movie_temp.movie_actors.create(actor: a)
      else
        d = Hash.new
        d['name'] = actor_1
        a = Actor.new(d)
        a.save()
        a1 = Actor.where(["name = :u", { u: actor_1 }]).first
        movie_temp.movie_actors.create(actor: a)
        
      end
        
    end
    if(!actor_2.nil?)
      actor_exists = Actor.exists?((["name = :u", { u: actor_2 }]))
      if(actor_exists)
        a = Actor.where(["name = :u", { u: actor_2 }]).first
        movie_temp.movie_actors.create(actor: a)
      else
        d = Hash.new
        d['name'] = actor_2
        a = Actor.new(d)
        a.save()
        a1 = Actor.where(["name = :u", { u: actor_2 }]).first
        movie_temp.movie_actors.create(actor: a)
        
      end
        
    end
    if(!actor_3.nil?)
      actor_exists = Actor.exists?((["name = :u", { u: actor_3 }]))
      if(actor_exists)
        a = Actor.where(["name = :u", { u: actor_3 }]).first
        movie_temp.movie_actors.create(actor: a)
      else
        d = Hash.new
        d['name'] = actor_3
        a = Actor.new(d)
        a.save()
        a1 = Actor.where(["name = :u", { u: actor_3 }]).first
        movie_temp.movie_actors.create(actor: a)
        
      end
        
    end

    
  end


  
end
